#!/bin/bash

# NOME
#   gravar - grava a sesión da terminal
# SINOPSIS
#   . gravar.sh delay filename
#   - delay (segundos de duración da gravación)
#   - filename (nome do ficheiro final, con extensión .gif)

echo "Selecciona unha xanela..."

byzanz-record $(xwininfo | awk '
  /Absolute upper-left X/ { x = $4 }
  /Absolute upper-left Y/ { y = $4 }
  /Width/                 { w = $2 }
  /Height/                { h = $2 }
  END                     { print "-x", x, "-y", y, "-w", w, "-h", h }
') -v --delay=2 --duration="$1" out.byzanz 

byzanz-playback out.byzanz $2.gif
